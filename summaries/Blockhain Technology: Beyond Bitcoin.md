# Blockchain Technology: Beyond Bitcoin

[Crosby (Google), Nachiappan (Yahoo), Pattanayak (Yahoo), Verma (Samsung Research America), Kalyanaraman (Fairchild Semiconductor) 2016](https://j2-capital.com/wp-content/uploads/2017/11/AIR-2016-Blockchain.pdf)

Blockchain es una tecnología que ha incrementado ampliamente su popularidad en los últimos años debido a que busca resolver los problemas de seguridad, confianza y privacidad de múltiples modelos de negocio que involucran el intercambio de bienes o servicios entre dos usuarios. Actualmente estos problemas son resueltos por un tercero que hace de intermediario y quien obtiene una ganancia por ayudar a llegar a un consenso entre los involucrados, sin embargo estos terceros no siempre garantizan la solución de los problemas de seguridad, el nivel deseado de privacidad o no ofrecen la transparencia necesaria para crear confianza en todos los involucrados.

Para explicar el funcionamiento de Blockchain el articulo se basa principalmente en la implementación realizada por el proyecto Bitcoin acompañado de Etherum que busca resolver de una forma mas general los problemas mencionados con el uso de los llamados "Smart Contracts" que permiten ejecutar acciones especificas al cumplir un grupo de condiciones acordadas por las partes involucradas. Posteriormente lista algunas de las aplicaciones que se están llevando acabo actualmente dividas en financieras y no financieras y por ultimo expone algunos de los riesgos que conlleva la adopción de estas tecnologías.

## Aplicaciones Financieras

 * [_NASDAQ private equity_](https://chain.com/sequence/): NASDAQ en conjunto con la startup chain.com lanzaron una plataforma privada de intercambio de "acciones", legalmente es equity, en una fase previa a la salida a la bolsa de las compañías que no requiere de la intervención de los bancos.

* [_Augur_](https://www.augur.net): Es una aplicación que permite vender acciones de forma anticipada a un evento que puede afectar el mercado sin la necesidad de cobrar comisiones altas o depender de brokers centralizados, adicionalmente el pago se hace a través de Etherum.

* [_Everledger_](https://www.everledger.io/about-us/about): Una compañía que utiliza blockchain para hacer seguimiento de las características y el historial de propietarios que ha tenido cada diamante.


## Aplicaciones no financieras

La certificación de documentos para diferentes efectos legales también esta siendo ampliamente impactada por las tecnologías de Blockchain, algunas de las empresas que están trabajando en prestar este tipo de servicios son:

  * [_Stampery_](https://stampery.com/)
  * [_Viacoin_](https://viacoin.org/)
  * [_Block Notary_](https://www.blocknotary.com/)
  * [_Crypto Public Notary_](https://www.ccn.com/crypto-public-notary-uses-bitcoin-block-chain-notarize-digital-content)
  * [_Proof of Existence_](https://proofofexistence.com/)
  * [_Ascribe_](www.ascribe.io)

Otras aplicaciones mencionadas en el articulo cubren diferentes campos como los derechos de autor que enfrentan crecientes problemas debido al aumento de las plataformas de streaming, sistemas que permitan controlar las falsificaciones en las cadenas de suministros, la necesidad de conectar múltiples dispositivos en una red independiente en redes cada vez mas grandes de IoT(Internet of Things) o el mantenimiento de registros como los DNS sin la necesidad de gobiernos o grandes compañías como intermediaros.

## Riesgos de Adopción

* Resistencia al cambio: Blockchain conlleva un cambio de paradigma en la forma en la que se realizan transacciones entre dos partes y esto puede generar resistencia por parte las grandes corporaciones dedicadas a resolver los problemas de confianza y privacidad como VISA y Mastercard, en el caso de las tarjetas de credito, e incluso por parte de los mismos usuarios.

* Escalabilidad: Con el paso del tiempo la cantidad de bloques crece y realizar una sola transacción puede resultar costoso a pesar del uso de herramientas como Merkle Tree en el caso de Bitcoin.

* Costos de Migración de datos: La aplicación de Blockchain en algunos de los campos mencionados incluye largos procesos de ETL como la migración de los contratos y documentos actuales en negocios como la propiedad raíz.

* Regulaciones: Actualmente no hay regulaciones para estas redes P2P por parte de los gobiernos y estos podrían poner trabas para su adopción.

* Actividades Fraudulentas: Si bien los usuarios suelen preocuparse por su privacidad por cuestiones personales o de seguridad también hay otro grupo de personas interesadas en mantener el anonimato para realizar actos ilegales.

* Computación Quántica: Gran parte de la seguridad de las redes basadas en Blockchain recae en la dificultad matemática de resolver puzzles criptográficos, pero con la llegada de la computación quántica puede que los algoritmos actuales no sean suficientemente costos a nivel de hardware para asegurar que una parte de los involucrados logre engañar el sistema a su favor.
