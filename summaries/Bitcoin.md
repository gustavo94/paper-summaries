# Bitcoin: A Peer-to-Peer Electronic Cash System
[Satoshi Nakamoto](https://bitcoin.org/bitcoin.pdf)

Bitcoin es la implementacion mas reconocida de blockchain y el whitepaper escrito por el autor anonimo Satoshi Nakamoto fue uno de los primeros en listar y describir todos los elementos que componen una implementacion de blockchain, la principal intencion de este articulo era proponer una moneda electronica que eliminara la necesidad de tener un tercero de confianza como intermediario de todas las transacciones como unico metodo para evitar el problema de doble gasto.

## Transacciones
Los bloques de bitcoin son una estructura de datos diseñada para almacenar las transacciones de dinero entre usuarios de la red para ello cada que un usuario desea transferir una moneda a otro crea una "Transaccion" que lleva una firma electronica compuesta del hash de la transaccion anterior de la moneda unido a la llave publica del usuario que recibira la transferencia; con esta firma el nuevo dueño de la moneda puede verificar que le han hecho a él la transferencia sin embargo no puede comprobar que el dueño anterior no haya realizado la misma transferencia a otro, esto es lo que se conoce como doble-gasto(double-spending problem). Adicionalmente las transacciones se componen de multiples entradas y multiples salidas para permitir combinar y separar grupos de monedas y que no sea necesario manejarlas individualmente.

## Proof of Work
La prueba de trabajo consiste en buscar un valor tal que al unirlo con un grupo el siguiente grupo de transacciones, o bloque, y aplicar una funcion de hash, como SHA-256, da como resultado un numero especifico de ceros al inicio del hash. El trabajo de computacion necesario para hallar este valor sirve para garantizar que un atacante no pueda crear un bloque con transacciones corruptas sin tener por lo menos el 51% de la capacidad de computacion de toda la red y a su vez sirve como argumento para que en el algoritmo de consenso los nodos siempre elijan la cadena de bloques mas larga en caso de recibir diferentes cadenas desde diferentes nodos.

Adicionalmente para controlar el aumento en la capacidad de los componentes de computacion la red se encuentra constantemente incrementando el numero de ceros requeridos para aumentar la dificultad y de esta forma exigir aun mas capacidad de computacion.

Este costo computacional de crear cada bloque nuevo tambien esta ligado a la creación de nuevas monedas añadiddas a la red, dando incentivos a aquellos que logran resolver el desafio a traves de la insersion de nuevas monedas y del cobro de una cuota por "honorarios" al valor de la transaccion. Se espera que llegue un punto donde los honorarios sean suficientes para cubrir los gastos de prestar este poder de computacion a la red.

## Network
Cada que se realiza una nueva transaccion esta es transmitida y añadida a la cadena de bloque siguiendo 6 pasos:
 1. La transaccion es emitida a todos los nodos
 2. Cada nodo agrupa las nuevas transacciones en un bloque
 3. Cada nodo trata de encontrar el valor que satisfaga la prueba de trabajo
 4. Cuando un nodo supera la prueba de trabajo emite el nuevo bloque a toda la red
 5. Los nodos aceptan los bloques siempre y cuando todas las transacciones sean validas y los bloques cumplan la prueba de trabajo
 6. Los nodos añaden el bloque que aceptaron y pasan a trabajar en el siguiente

## Merkle Tree
Con el paso del tiempo la cantidad de transacciones dentro de la red podrian llegar a ocupar demasiado espacio en disco si se mantiene todo el historial desde el inicio de la red, por esta razon se utiliza un Merkle Tree como estructura de datos para ordenar los hashes de las diferentes transacciones dentro de un bloque, de este modo cuando hay suficientes transacciones sobre una moneda las mas viejas pueden ser eliminadas del arbol sin afectar la cadena de bloques manteniendo unicamente la raiz del bloque.

## Privacy
Dentro de la red todas las transacciones son publicas sin embargo lo unico que puede observarse en ellas es la cantidad de valor que se esta transfiriendo y la llave publica de aquel que recibe este valor sin embargo la identidad de las personas detras de estas transacciones permanece oculta.
