# Distilling the Knowledge in a Neural Network
(Destilando el Conocimiento en una Red Neuronal)

[arXiv:1503.02531](http://arxiv.org/abs/1503.02531v1) \[stat.ML\]

Generalmente, las redes neuronales reciben como entrada un tensor que representa un objeto y producen como salida la probabilidad estimada por la red de que este objeto pertenezca a cada clase de un conjunto de clases que se usaron para  entrenar el modelo.

```
. . .                    clase 1: ---
. . . => Red Neuronal => clase 2: -------
. . .                    clase 3: -
entrada                  clase 4: ----
```

En el ejemplo anterior, la red neuronal afirma que la entrada probablemente pertenezca a la clase 2, pero la distribución de probabilidades muestra más que eso: también indica que el objeto analizado comparte ciertas características con objetos de las clases 1 y 4, pero no es muy similar a ninguno que conozca de la clase 3. Sin embargo, durante el entrenamiento de la red no es usual mostrar este tipo de relaciones entre clases, solo se le indica a la red "este objeto pertenece a la clase 2." Estas relaciones son el resultado de entrenar la red con miles de ejemplos y muestran la capacidad de la red de generalizar el conocimiento que se le enseña.

Es común que las redes neuronales con mejor desempeño en su tarea de clasificar estén compuestas por millones de parámetros, lo que no las hace muy prácticas cuando se busca desplegarlas para ser usadas en el "Mundo real," por ejemplo, en celuares. Para hacer que las redes sean realmente útiles es necesario *transformarlas* en modelos más pequeños que puedan ser utilizados por fuera de clusters de supercomputadoras; pero en la práctica, al entrenar estos modelos más pequeños con el mismo conjunto de entrenamiento que se usa para entrenar los modelos *grandes* se obtienen resultados pobres.

En el artículo, Hinton et. al. proponen entrenar estos modelos pequeños no solo con los objetos marcados para una única clase sino también con las salidas de un modelo *grande*: ya no se le enseñará al modelo pequeño "este objeto pertenece a la clase 2" sino "este objeto pertenece a la clase 2 y es similar a objetos de las clases 1 y 4." De esta forma, se logra transferir mucho más conocimiento al modelo pequeño y se obtienen resultados mucho mejores que al entrenarlo con una sola clase objetivo para cada entrada.

Los resultados experimentales de los autores para tareas de reconocimiento de caracteres manuscritos (en el dataset MNIST) y para reconocimiento de voz muestranque los modelos pequeños entrenados con esta técnica logran retener la mayor parte del conocimiento adquirido por los modelos más grandes.
